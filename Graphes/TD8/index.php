<?php

include "../TD7/index.php";

/**
 * @param array $matrix
 */
function transposeMatrix($matrix) {
    $out = array();
    foreach ($matrix as $key => $subarr) {
        foreach ($subarr as $subkey => $subvalue) {
            $out[$subkey][$key] = $subvalue;
        }
    }
    return $out;
}

/**
 * @param array $matrixA
 * @param array $matrixB
 * @return array
 */
function sumOfMatrix($matrixA, $matrixB){
    $res = [];
    foreach ($matrixA as $key => $subarr) {
        foreach ($subarr as $subkey => $subvalue) {
            $res[$key][$subkey] = $matrixA[$key][$subkey] + $matrixB[$key][$subkey];
        }
    }
    return $res;
}

/**
 * @param array $matrix
 * @return array
 */
function matriceAdjacenteFermetureSymetrique($matrix){
    $tMatrix = transposeMatrix($matrix);
    $res = sumOfMatrix($matrix, $tMatrix);
    return $res;
}

/**
 * @param array $array
 */
function displayTable($array)
{
    $disp = '<table class="table" style="border: 1px solid black">';
    foreach ($array as $tr) {
        $disp .= '<tr style="border: 1px solid black">';
        foreach ($tr as $td) {
            $disp .= '<td style="border: 1px solid black">' . $td . '</td>';
        }
        $disp .= '</tr>';
    }
    $disp .= '</table>';
    echo $disp;
}

/**
 * @param Graph $graph
 * @return array
 */
function matrixFromGraph($graph){
    $currentPoints = new SplQueue();
    $currentPoints->push($graph);

    $result = array();
    while(!$currentPoints->isEmpty()){
        $currentPoint = $currentPoints->pop();

        if(!$currentPoint->isAlreadyDone()){
            $result[] = $currentPoint->getId();
            foreach ($currentPoint->getDestinations() as $point){
                $currentPoints->push($point);
                $result[$currentPoint->getId()] = array();
                $result[$currentPoint->getId()][$point->getId()] = 1;
            }
            $currentPoint->setAlreadyDone(true);
        }
    }
    return $result;
}

$matrixData = [
    [0, 1, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 0],
    [0, 0, 0, 0, 1, 0],
    [0, 1, 0, 0, 0, 0],
    [0, 0, 0, 1, 0, 1],
    [0, 0, 0, 0, 0, 0],
];

$res1 = matriceAdjacenteFermetureSymetrique($matrixData);

echo " MATRICE ADJACENCE GRAPH FERMETURE SYMETRIQUE";
displayTable($res1);

$graph = buildFromMatrix($matrixData);
$res2 = matrixFromGraph($graph[0]);
echo "<hr>";
echo " MATRICE ADJACENCE DEPUIS GRAPH";
displayTable($res2);