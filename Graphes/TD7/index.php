<?php

include "../TD6/index.php";

/******************************
 * PARCOURS
 *****************************/

/**
 * @param Graph $graph
 * @return array
 */
function parcoursLargeur($graph){
    $currentPoints = new SplQueue();
    $currentPoints->push($graph);

    $result = [];
    while(!$currentPoints->isEmpty()){
        $currentPoint = $currentPoints->pop();

        if(!$currentPoint->isAlreadyDone()){
            $result[] = $currentPoint->getId();
            foreach ($currentPoint->getDestinations() as $point){
                $currentPoints->push($point);
            }
            $currentPoint->setAlreadyDone(true);
        }
    }
    return $result;
}

/**
 * @param Graph $graph
 * @param array $result
 * @return array
 */
function parcourProfondeur($graph, &$result){
    if(!$graph->isAlreadyDone()){
        $result[] = $graph->getId();
        $graph->setAlreadyDone(true);
        foreach ($graph->getDestinations() as $point){
            parcourProfondeur($point, $result);
        }
        return $result;
    }
}

/******
 * Tests des fonctions
 ******/

$matrixData = [
    [0, 1, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 0],
    [0, 0, 0, 0, 1, 0],
    [0, 1, 0, 0, 0, 0],
    [0, 0, 0, 1, 0, 1],
    [0, 0, 0, 0, 0, 0],
];

$listOfNode = buildFromMatrix($matrixData);
$listOfNode2 = buildFromMatrix($matrixData);

/****************************
 * AFFICHAGE
 ***************************/

echo "\n PARCOURS LARGEUR GRAPHES \n";

echo "<br/>";
foreach ($parcoursL = parcoursLargeur($listOfNode[0]) as $sommet){
    echo $sommet . ',';
}

echo "<hr>";
echo "\n  PARCOURS PROF GRAPHES \n";
echo "<br/>";
$result = [];
foreach ($parcoursP = parcourProfondeur($listOfNode2[0], $result) as $sommet){
    echo $sommet . ',';
}
echo "<hr>";