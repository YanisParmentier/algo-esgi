<?php

class Graph {

    /**
     * @var int $id
     */
    private $id;

    /**
     * @var Graph[]
     */
    private $destinations = [];

    /**
     * @var boolean
     */
    private $alreadyDone;

    /**
     * @return bool
     */
    public function isAlreadyDone()
    {
        return $this->alreadyDone;
    }

    /**
     * @param bool $alreadyDone
     */
    public function setAlreadyDone($alreadyDone)
    {
        $this->alreadyDone = $alreadyDone;
    }

    /**
     * @var int $id
     * Graph constructor.
     */
    public function __construct($id)
    {
        $this->id = $id;
        $this->alreadyDone = false;
    }



    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Graph[]
     */
    public function getDestinations()
    {
        return $this->destinations;
    }

    /**
     * @param Graph[] $destinations
     */
    public function setDestinations($destinations)
    {
        $this->destinations = $destinations;
    }

    /**
     * @param Graph $destination
     */
    public function addDestination($destination)
    {
        $this->destinations[] = $destination;
    }
}

/**
 * @param int[][] $matrix
 * @return array
 */
function buildFromMatrix($matrix){
    $listOfNode = [];
    foreach ($matrix as $nbrow=>$row){
        $listOfNode[] = new Graph($nbrow);
    }
    foreach ($matrix as $nbrow=>$row){
        foreach ($row as $nbcol=>$col){
            if($col == 1){
                $listOfNode[$nbrow]->addDestination($listOfNode[$nbcol]);
            }
        }
    }
    return $listOfNode;
}


$matrixData = [
    [0, 1, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 0],
    [0, 0, 0, 0, 1, 0],
    [0, 1, 0, 0, 0, 0],
    [0, 0, 0, 1, 0, 1],
    [0, 0, 0, 0, 0, 0],
];

buildFromMatrix($matrixData);
