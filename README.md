# Algo ESGI

**TD/TP et projets d'algo**


- Tout le code est en PHP.
- Installer PHP 7.4.9
- Définir la variable d'environnement pour PHP

**- Projet Graphes :**
- L'interface du projet est une page web, il faut un serveur web (Wamp) pour la tester
1. Ouvrir la page [/Projet-Graphes/index.php](https://gitlab.com/YanisParmentier/algo-esgi/-/blob/main/Projet-Graphes/index.php)
2. Selectionner un lieu de départ et un lieu d'arrivée parmis ceux présents sur le graphe qui s'affiche sur la page
3. Appuyer sur le bouton "Envoyer"
4. Observer les résultats (Chemin, temps de trajet, temps de calcul)
5. Appuyer sur "Choisir un nouvel itinéraire" pour recommencer

- Les poids sont implémentés (ils sont à 1 pour correspondre à l'image du graphe)
- L'algorithme de Dijkstra fonctionne correctement
- L'algorithme de Bellman Ford est implémenté mais ne fonctionne pas parfaitement, je n'ai pas trouvé comment le faire fonctionner à 100% avec mon implémentation de graphe.


**- TD1 :**
1. Code : Exécuter le fichier [/TD1/index.php](https://gitlab.com/YanisParmentier/algo-esgi/-/blob/main/TD1/index.php). (php /TD1/index.php)

2. Les réponses aux exercices se trouvent dans le fichier TD1.txt 

**- TD2 :**

1. Les réponses aux exercices se trouvent dans le fichier TD2.txt 

**- TD3 :**
1. Code : Exécuter le fichier [/TD3/index.php](https://gitlab.com/YanisParmentier/algo-esgi/-/blob/main/TD3/index.php). (php /TD3/index.php)

**- TD4 :**
1. Code : Exécuter le fichier [/TD4/index.php](https://gitlab.com/YanisParmentier/algo-esgi/-/blob/main/TD4/index.php). (php /TD4/index.php)




