<?php


/**
 * Class Tree
 * @param $value string
 * @param $children Tree[]
 */
class Tree
{
    private $value;
    private $children = array();

    /**
     * Tree constructor.
     * @param $value string
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @return Tree[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param $node Tree
     */
    public function addNode($node)
    {
        $this->children[] = $node;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }


}

/**
 * Class Tree
 * @param $value string
 * @param $nodeL BinaryTree
 * @param $nodeR BinaryTree
 */
class BinaryTree
{
    private $value;
    private $nodeL;
    private $nodeR;

    /**
     * Tree constructor.
     * @param $value string
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @return BinaryTree
     */
    public function getLeftNode()
    {
        return $this->nodeL;
    }

    /**
     * @return BinaryTree
     */
    public function getRightNode()
    {
        return $this->nodeR;
    }

    /**
     * @param $node BinaryTree
     */
    public function addLeftNode($node)
    {
        $this->nodeL = $node;
    }

    /**
     * @param $node BinaryTree
     */
    public function addRightNode($node)
    {
        $this->nodeR = $node;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }


}

/**
 * @param $tree Tree
 * @param $result array
 */
function parcoursProfondeurPrefix($tree, &$result)
{

    $result[] = $tree->getValue();

    foreach ($tree->getChildren() as $child) {
        parcoursProfondeurPrefix($child, $result);
    }
    return $result;

}

/**
 * @param $tree Tree
 * @param $result array
 */
function parcoursProfondeurSuffix($tree, &$result)
{

    foreach ($tree->getChildren() as $child) {
        parcoursProfondeurSuffix($child, $result);
    }
    $result[] = $tree->getValue();
    return $result;

}

/**
 * @param $tree Tree
 */
function parcoursLargeur($tree)
{
    $currentNodes = new SplQueue();
    $currentNodes->push($tree);
    $result = [];
    while (!$currentNodes->isEmpty()) {
        $currentNode = $currentNodes->shift();
        $result[] = $currentNode->getValue();
        foreach ($currentNode->getChildren() as $child) {
            $currentNodes->push($child);
        }
    }
    return $result;
}

/**
 * @param $tree BinaryTree
 * @param $result array
 */
function parcoursInfixe($tree, &$result)
{
    if ($tree->getLeftNode()) {
        parcoursInfixe($tree->getLeftNode(), $result);
    }

    $result[] = $tree->getValue();
    if ($tree->getRightNode()) {
        parcoursInfixe($tree->getRightNode(), $result);
    }

    return $result;
}

/*****************************************
 ** ARBRE EXO 1
 ****************************************/

$treeExo1 = new Tree('A');
$treeExo1->addNode($nodeB = new Tree('B'));
$treeExo1->addNode($nodeC = new Tree('C'));
$treeExo1->addNode($nodeD = new Tree('D'));
$nodeB->addNode($nodeE = new Tree('E'));
$nodeB->addNode($nodeF = new Tree('F'));
$nodeE->addNode($nodeJ = new Tree('J'));
$nodeE->addNode($nodeK = new Tree('K'));
$nodeE->addNode($nodeL = new Tree('L'));
$nodeD->addNode($nodeG = new Tree('G'));
$nodeD->addNode($nodeH = new Tree('H'));
$nodeD->addNode($nodeI = new Tree('I'));
$nodeG->addNode($nodeM = new Tree('M'));
$nodeM->addNode($nodeP = new Tree('P'));
$nodeI->addNode($nodeN = new Tree('N'));
$nodeI->addNode($nodeO = new Tree('O'));

/*****************************************
 ** ARBRE EXO 2
 ****************************************/

$treeExo2 = new Tree('20');
$treeExo2->addNode($node5 = new Tree('5'));
$treeExo2->addNode($node25 = new Tree('25'));
$node5->addNode($node3 = new Tree('3'));
$node5->addNode($node12 = new Tree('12'));
$node12->addNode($node8 = new Tree('8'));
$node12->addNode($node13 = new Tree('13'));
$node8->addNode($node6 = new Tree('6'));
$node25->addNode($node21 = new Tree('21'));
$node25->addNode($node28 = new Tree('28'));

/*****************************************
 ** ARBRE BINAIRE EXO 2
 ****************************************/

$binaryTreeExo2 = new BinaryTree('20');
$binaryTreeExo2->addLeftNode($node5 = new BinaryTree('5'));
$binaryTreeExo2->addRightNode($node25 = new BinaryTree('25'));
$node5->addLeftNode($node3 = new BinaryTree('3'));
$node5->addRightNode($node12 = new BinaryTree('12'));
$node12->addLeftNode($node8 = new BinaryTree('8'));
$node12->addRightNode($node13 = new BinaryTree('13'));
$node8->addLeftNode($node6 = new BinaryTree('6'));
$node25->addLeftNode($node21 = new BinaryTree('21'));
$node25->addRightNode($node28 = new BinaryTree('28'));


/**************************************
 * Parcours exos
 **************************************/

///EXO 1
$result = array();
$arrayPrefix1 = parcoursProfondeurPrefix($treeExo1,$result);

$result = array();
$arraySuffix1 = parcoursProfondeurSuffix($treeExo1,$result);

$arrayLargeur1 = parcoursLargeur($treeExo1);


//EXO 2
$result = array();
$arrayPrefix2 = parcoursProfondeurPrefix($treeExo2,$result);

$result = array();
$arraySuffix2 = parcoursProfondeurSuffix($treeExo2,$result);

$arrayLargeur2 = parcoursLargeur($treeExo2);

$result = array();
$arrayInfix2 = parcoursInfixe($binaryTreeExo2,$result);

/**************************************
 * AFFICHAGE
 *************************************/

echo "\n //////////// LARGEUR EXO 1 //////////// \n";
echo implode(',',$arrayLargeur1);
echo "\n\n //////////// PREFIXE EXO 1 //////////// \n";
echo implode(',',$arrayPrefix1);
echo "\n\n //////////// SUFFIXE EXO 1 //////////// \n";
echo implode(',',$arraySuffix1);
echo "\n\n //////////// LARGEUR EXO 2 //////////// \n";
echo implode(',',$arrayLargeur2);
echo "\n\n //////////// PREFIXE EXO 2 //////////// \n";
echo implode(',',$arrayPrefix2);
echo "\n\n //////////// SUFFIXE EXO 2 //////////// \n";
echo implode(',',$arraySuffix2);
echo "\n\n //////////// INFIXE EXO 2 //////////// \n";
echo implode(',',$arrayInfix2);

