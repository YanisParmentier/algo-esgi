<?php

include "abr.php";

$bst1 = new BinarySearchTree(20);
$bst1->addLeftNode($node5 = new BinarySearchTree(5));
$bst1->addRightNode($node25 = new BinarySearchTree(25));
$node5->addLeftNode($node3 = new BinarySearchTree(3));
$node5->addRightNode($node12 = new BinarySearchTree(12));
$node12->addLeftNode($node8 = new BinarySearchTree(8));
$node12->addRightNode($node13 = new BinarySearchTree(13));
$node8->addLeftNode($node6 = new BinarySearchTree(6));
$node25->addLeftNode($node21 = new BinarySearchTree(21));
$node25->addRightNode($node28 = new BinarySearchTree(28));


echo $bst1->nodeBalanceFactor();
echo "\n";
echo $bst1->isBalanced();