<?php

/**
 * Class Tree
 * @param $value int
 * @param $nodeL BinarySearchTree
 * @param $nodeR BinarySearchTree
 */
class BinarySearchTree
{
    private $value;
    private $nodeL;
    private $nodeR;

    /**
     * Tree constructor.
     * @param $value int
     */
    public function __construct($value = null)
    {
        $this->value = $value;
    }

    /**
     * @return BinarySearchTree
     */
    public function getLeftNode()
    {
        return $this->nodeL;
    }

    /**
     * @return BinarySearchTree
     */
    public function getRightNode()
    {
        return $this->nodeR;
    }

    /**
     * @param $node BinarySearchTree
     */
    public function addLeftNode($node)
    {
        $this->nodeL = $node;
    }

    /**
     * @param $node BinarySearchTree
     */
    public function addRightNode($node)
    {
        $this->nodeR = $node;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param $elementInserted BinarySearchTree
     */
    public function insertElement($elementInserted){

        if($this->value !== null) {
            if ($elementInserted->value < $this->value) {
                if ($this->getLeftNode()) {
                    $this->getLeftNode()->insertElement($elementInserted);
                } else {
                    $this->addLeftNode($elementInserted);
                }
            }

            if ($elementInserted->value > $this->value) {
                if ($this->getRightNode()) {
                    $this->getRightNode()->insertElement($elementInserted);
                } else {
                    $this->addRightNode($elementInserted);
                }
            }
        } else {
            $this->value = $elementInserted->value;
        }
    }

    /**
     * @param $elementSearched int
     * @param $lastValue int
     */
    public function searchElement($elementSearched){

        if (!$this->value){
            return null;
        }
        if ($elementSearched < $this->value) {
            if ($this->getLeftNode()) {
                return $this->getLeftNode()->searchElement($elementSearched);
            }

        } elseif($elementSearched > $this->value) {
            if ($this->getRightNode()) {
                return $this->getRightNode()->searchElement($elementSearched);
            }
        } else {
            return $this->value == $elementSearched;
        }
//            return $lastValue == $elementSearched ? $lastValue : "L'élément n'exsite pas, la dernière étiquette parcourue est " . $lastValue;
    }

    /**
     * @param $tree BinarySearchTree
     * @param $result array
     */
    public function parcoursInfixe(&$result){
        if($this->getLeftNode()){
            $this->getLeftNode()->parcoursInfixe($result);
        }

        $result[] = $this->getValue();
        if($this->getRightNode()){
            $this->getRightNode()->parcoursInfixe($result);
        }

        return $result;
    }

    /**
     * @param $elementsList array
     */
    public function createTreeFromList($elementsList){
        foreach ($elementsList as $element){
            $this->insertElement(new BinarySearchTree($element));
        }
    }

}