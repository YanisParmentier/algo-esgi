<?php

include "abr.php";

/*****************************************
 ** ARBRE BINAIRE DE RECHERCHE EXO 1
 ****************************************/

$bst1 = new BinarySearchTree(20);
$bst1->addLeftNode($node5 = new BinarySearchTree(5));
$bst1->addRightNode($node25 = new BinarySearchTree(25));
$node5->addLeftNode($node3 = new BinarySearchTree(3));
$node5->addRightNode($node12 = new BinarySearchTree(12));
$node12->addLeftNode($node8 = new BinarySearchTree(8));
$node12->addRightNode($node13 = new BinarySearchTree(13));
$node8->addLeftNode($node6 = new BinarySearchTree(6));
$node25->addLeftNode($node21 = new BinarySearchTree(21));
$node25->addRightNode($node28 = new BinarySearchTree(28));

$bst1->insertElement($node50 = new BinarySearchTree(50));
$bst1->insertElement($node5 = new BinarySearchTree(5));
$bst1->insertElement($node25 = new BinarySearchTree(25));
$bst1->insertElement($node19 = new BinarySearchTree(19));


/*****************************************
 ** ARBRE BINAIRE DE RECHERCHE EXO 2
 ****************************************/

$bst2 = new BinarySearchTree();
$bst2->insertElement(new BinarySearchTree(25));
$bst2->insertElement(new BinarySearchTree(60));
$bst2->insertElement(new BinarySearchTree(35));
$bst2->insertElement(new BinarySearchTree(10));
$bst2->insertElement(new BinarySearchTree(5));
$bst2->insertElement(new BinarySearchTree(20));
$bst2->insertElement(new BinarySearchTree(65));
$bst2->insertElement(new BinarySearchTree(45));
$bst2->insertElement(new BinarySearchTree(70));
$bst2->insertElement(new BinarySearchTree(40));

function searchInList($list, $value){
    foreach ($list as $elem){
        if ($elem == $value){
            return true;
        }
    }
}

//10000 random entries array
$randomList= [];
for($i=0; $i<10000 ; $i++)
{
    $randomList[] = rand(1,10000) ;
}

$bst3 = new BinarySearchTree();
$bst3->createTreeFromList($randomList);



/**************************************
 * Parcours exos
 **************************************/

$result = array();
$arrayInfix1 = $bst1->parcoursInfixe($result);

$result = array();
$arrayInfix2 = $bst2->parcoursInfixe($result);

$result = array();
$arrayInfix3 = $bst3->parcoursInfixe($result);



/**************************************
 * AFFICHAGE
 *************************************/

echo "\n //////////// INFIXE EXO 1 //////////// \n";
echo implode(',',$arrayInfix1);

echo "\n //////////// INFIXE EXO 2 //////////// \n";
echo implode(',',$arrayInfix2);

echo "\n //////////// INFIXE EXO 2 //////////// (décommenter le echo en dessous pour afficher le tableau immense) \n";
//echo implode(',',$arrayInfix3);
echo $bst3->searchElement(100);

$start = microtime(true);
for($i=0; $i<100 ; $i++) {
    $bst3->searchElement(rand(1,10000));
}
$time_elapsed_secs = microtime(true) - $start;

echo "\n //////////// TEMPS EXEC POUR 100 RECHERCHES ALEATOIRES DANS UN ABR  ////////////\n";
echo $time_elapsed_secs;


$start2 = microtime(true);
for($i=0; $i<100 ; $i++) {
    searchInList($randomList,rand(1,10000));
}
$time_elapsed_secs2 = microtime(true) - $start2;

echo "\n //////////// TEMPS EXEC POUR 100 RECHERCHES ALEATOIRES DANS UNE LISTE ////////////\n";
echo $time_elapsed_secs2;
