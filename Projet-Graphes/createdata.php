<?php

include "./graph.php";

$leport = new Graph('Le Port');
$lapossession = new Graph('La Possession');
$saintpaul = new Graph('Saint-Paul');
$troisbassins = new Graph('Trois-Bassins');
$saintleu = new Graph('Saint-Leu');
$lesavirons = new Graph('Les Avirons');
$etangsale = new Graph('Etang-salé');
$lariviere = new Graph('La Rivière');
$cilaos = new Graph('Cilaos');
$saintlouis = new Graph('Saint-Louis');
$saintpierre = new Graph('Saint-Pierre');
$entredeux = new Graph('Entre-Deux');
$petiteile = new Graph('Petite-Ile');
$letampon = new Graph('Le Tampon');
$saintjoseph = new Graph('Saint-Joseph');
$saintphilippe = new Graph('Saint-Philippe');
$sainterose = new Graph('Sainte-Rose');
$saintbenoit = new Graph('Saint-Benoit');
$plainedespalmistes = new Graph('Plaine-des-Palmistes');
$braspanon = new Graph('Bras-Panon');
$saintandre = new Graph('Saint-André');
$salazie = new Graph('Salazie');
$saintesuzanne = new Graph('Sainte-Suzanne');
$saintemarie = new Graph('Sainte-Marie');
$saintdenis = new Graph('Saint-Denis');


$leport->addBranche(new Branche(1,$lapossession));
$lapossession->addBranche(new Branche(1,$leport));
$lapossession->addBranche(new Branche(1,$saintpaul));
$lapossession->addBranche(new Branche(1,$saintdenis));
$saintpaul->addBranche(new Branche(1,$lapossession));
$saintpaul->addBranche(new Branche(1,$troisbassins));
$saintpaul->addBranche(new Branche(1,$lesavirons));
$troisbassins->addBranche(new Branche(1,$saintpaul));
$troisbassins->addBranche(new Branche(1,$saintleu));
$saintleu->addBranche(new Branche(1,$troisbassins));
$saintleu->addBranche(new Branche(1,$lesavirons));
$lesavirons->addBranche(new Branche(1,$saintleu));
$lesavirons->addBranche(new Branche(1,$saintpaul));
$lesavirons->addBranche(new Branche(1,$etangsale));
$etangsale->addBranche(new Branche(1,$lesavirons));
$etangsale->addBranche(new Branche(1,$lariviere));
$etangsale->addBranche(new Branche(1,$saintlouis));
$lariviere->addBranche(new Branche(1,$etangsale));
$lariviere->addBranche(new Branche(1,$cilaos));
$lariviere->addBranche(new Branche(1,$saintlouis));
$cilaos->addBranche(new Branche(1,$lariviere));
$saintlouis->addBranche(new Branche(1,$etangsale));
$saintlouis->addBranche(new Branche(1,$lariviere));
$saintlouis->addBranche(new Branche(1,$saintpierre));
$saintpierre->addBranche(new Branche(1,$saintlouis));
$saintpierre->addBranche(new Branche(1,$entredeux));
$saintpierre->addBranche(new Branche(1,$letampon));
$saintpierre->addBranche(new Branche(1,$petiteile));
$entredeux->addBranche(new Branche(1,$saintpierre));
$petiteile->addBranche(new Branche(1,$saintpierre));
$petiteile->addBranche(new Branche(1,$letampon));
$petiteile->addBranche(new Branche(1,$saintjoseph));
$letampon->addBranche(new Branche(1,$saintpierre));
$letampon->addBranche(new Branche(1,$petiteile));
$letampon->addBranche(new Branche(1,$saintpierre));
$letampon->addBranche(new Branche(1,$plainedespalmistes));
$letampon->addBranche(new Branche(1,$petiteile));
$plainedespalmistes->addBranche(new Branche(1,$letampon));
$plainedespalmistes->addBranche(new Branche(1,$saintbenoit));
$saintjoseph->addBranche(new Branche(1,$petiteile));
$saintjoseph->addBranche(new Branche(1,$saintphilippe));
$saintphilippe->addBranche(new Branche(1,$saintjoseph));
$saintphilippe->addBranche(new Branche(1,$sainterose));
$sainterose->addBranche(new Branche(1,$saintphilippe));
$sainterose->addBranche(new Branche(1,$saintbenoit));
$saintbenoit->addBranche(new Branche(1,$plainedespalmistes));
$saintbenoit->addBranche(new Branche(1,$sainterose));
$saintbenoit->addBranche(new Branche(1,$braspanon));
$braspanon->addBranche(new Branche(1,$saintbenoit));
$braspanon->addBranche(new Branche(1,$saintandre));
$saintandre->addBranche(new Branche(1,$braspanon));
$saintandre->addBranche(new Branche(1,$salazie));
$saintandre->addBranche(new Branche(1,$saintesuzanne));
$salazie->addBranche(new Branche(1,$saintandre));
$saintesuzanne->addBranche(new Branche(1,$saintandre));
$saintesuzanne->addBranche(new Branche(1,$saintemarie));
$saintemarie->addBranche(new Branche(1,$saintesuzanne));
$saintemarie->addBranche(new Branche(1,$saintdenis));
$saintdenis->addBranche(new Branche(1,$saintemarie));
$saintdenis->addBranche(new Branche(1,$lapossession));

$listNoeuds = parcoursLargeurListOfNodes($leport);



