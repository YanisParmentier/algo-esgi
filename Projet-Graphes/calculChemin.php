<?php

include "./createdata.php";

if (!empty($_POST["depart"])) {
    $depart = $_POST['depart'];
} else {
    echo "Veuillez choisir une station de départ : ";
    echo "<button><a href='./index.php'>Choisir un itinéraire</a></button>";
    die();
}

if (!empty($_POST["arrivee"])) {
    $arrivee = $_POST['arrivee'];
} else {
    echo "Veuillez choisir une station d'arrivée : ";
    echo "<button><a href='./index.php'>Choisir un itinéraire</a></button>";
    die();
}

$noeudDepart = getNoeud($depart,$listNoeuds);
$noeudArrivee = getNoeud($arrivee,$listNoeuds);

$startDijkstra = microtime(true);
$resDijkstra = plusCourtCheminDijkstra($noeudDepart,$noeudArrivee,$listNoeuds);
$tempsDijkstra = microtime(true) - $startDijkstra;

$startBF = microtime(true);
$resBF = plusCourtCheminBellmanFord($noeudDepart,$noeudArrivee,$listNoeuds);
$tempsBF = microtime(true) - $startBF;

/*foreach ($listofnodes as $sommet){
    echo $sommet->distanceDeLaSource . ',';
}*/
echo "<br/>";
echo "<br/>";

echo '<img width="50%" src="./graphe.png">';


echo "<br/>";
echo 'Départ : ' . $depart;
echo "<br/>";
echo 'Arrivée : ' . $arrivee;
echo "<br/>";


echo "<br/>";
echo 'Résultats Dijkstra : ';
echo "<br/>";
echo 'Chemin : ';
echo implode(" - ",$resDijkstra['chemin']);

echo "<br/>";
echo "Distance : ";
echo $resDijkstra['distance'];

echo "<br/>";
echo "Temps de calcul : ";
echo $tempsDijkstra . " microsecondes.";


echo "<br/>";
echo "<br/>";
echo 'Résultats Bellman Ford : ';
echo "<br/>";
echo 'Chemin : ';
echo implode(" - ",$resBF['chemin']);

echo "<br/>";
echo "Distance : ";
echo $resBF['distance'];

echo "<br/>";
echo "Temps de calcul : ";
echo $tempsBF . " microsecondes.";


echo "<br/>";
echo "<br/>";
echo "Choisir un nouvel intinéraire : ";
echo "<button><a href='./index.php'>Choisir un itinéraire</a></button>";

