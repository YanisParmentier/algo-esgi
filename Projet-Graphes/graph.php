<?php



//class Dijkstra {
//    /**
//     * @var Graph
//     */
//    public $node;
//
//    /**
//     * @var boolean
//     */
//    public $visited;
//
//    /**
//     * @var double
//     */
//    public $distanceDeLaSource;
//
//    /**
//     * @var Graph
//     */
//    public $meilleurParentDepuisSource;
//
//    /**
//     * Dijkstra constructor.
//     * @param Graph $node
//     * @param bool $visited
//     * @param float $distanceDeLaSource
//     * @param null|Graph $meilleurParentDepuisSource
//     */
//    public function __construct(Graph $node, bool $visited, float $distanceDeLaSource, Graph $meilleurParentDepuisSource)
//    {
//        $this->node = $node;
//        $this->visited = $visited;
//        $this->distanceFromSource = $distanceDeLaSource;
//        $this->bestParentFromSource = $meilleurParentDepuisSource;
//    }
//}







class Branche {

    /**
     * @var int
     */
    public $poids;

    /**
     * @var Graph
     */
    public $destination;

    /**
     * Branche constructor.
     * @param int $poids
     * @param Graph $destination
     */
    public function __construct(int $poids, Graph $destination)
    {
        $this->poids = $poids;
        $this->destination = $destination;
    }

    /**
     * @return int
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * @param int $poids
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;
    }

    /**
     * @return Graph
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param Graph $destination
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
    }
}

class Graph {

    /**
     * @var boolean
     */
    public $visited;

    /**
     * @var double
     */
    public $distanceDeLaSource;

    /**
     * @var null|Graph
     */
    public $meilleurParentDepuisSource;

    /**
     * @var string $id
     */
    public $id;

    /**
     * @var Branche[]
     */
    public $branches = [];

    /**
     * @var boolean
     */
    public $alreadyDone;

    /**
     * @var string $id
     * Graph constructor.
     */
    public function __construct($id)
    {
        $this->id = $id;
        $this->alreadyDone = false;
        $this->visited = false;
        $this->distanceDeLaSource = INF;
        $this->meilleurParentDepuisSource = null;
    }

    /**
     * @return bool
     */
    public function isAlreadyDone()
    {
        return $this->alreadyDone;
    }

    /**
     * @param bool $alreadyDone
     */
    public function setAlreadyDone($alreadyDone)
    {
        $this->alreadyDone = $alreadyDone;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Branche[]
     */
    public function getBranches(): array
    {
        return $this->branches;
    }

    /**
     * @param Branche[] $branches
     */
    public function setBranches(array $branches): void
    {
        $this->branches = $branches;
    }

    /**
     * @param Graph $destination
     */
    public function addBranche($branche)
    {
        $this->branches[] = $branche;
    }
}


/******************************
 * PARCOURS
 *****************************/

/**
 * @param Graph $graph
 * @return array
 */
function parcoursLargeur($graph){
    $currentPoints = new SplQueue();
    $currentPoints->push($graph);

    $result = [];
    while(!$currentPoints->isEmpty()){
        $currentPoint = $currentPoints->pop();

        if(!$currentPoint->isAlreadyDone()){
            $result[] = $currentPoint->getId();
            foreach ($currentPoint->getBranches() as $branche){
                $currentPoints->push($branche->getDestination());
            }
            $currentPoint->setAlreadyDone(true);
        }
    }
    return $result;
}

/**
 * @param Graph $graph
 * @return Graph[]
 */
function parcoursLargeurListOfNodes($graph){
    $currentPoints = new SplQueue();
    $currentPoints->push($graph);

    $result = [];
    while(!$currentPoints->isEmpty()){
        $currentPoint = $currentPoints->pop();

        if(!$currentPoint->isAlreadyDone()){
            $result[] = $currentPoint;
            foreach ($currentPoint->getBranches() as $branche){
                $currentPoints->push($branche->getDestination());
            }
            $currentPoint->setAlreadyDone(true);
        }
    }
    return $result;
}

/**
 * @param Graph $graph
 * @param array $result
 * @return array
 */
function parcourProfondeur($graph, &$result){
    if(!$graph->isAlreadyDone()){
        $result[] = $graph->getId();
        $graph->setAlreadyDone(true);
        foreach ($graph->getBranches() as $branche){
            parcourProfondeur($branche->getDestination(), $result);
        }
        return $result;
    }
}

/**
 * @param Graph[] $listeNoeuds
 * @return Graph
 */
function rechercheNoeudPlusPetiteDistanceNonVisite(&$listeNoeuds){
    $dist = INF;
    $res = null;
    foreach ( $listeNoeuds as $element ) {
        if ( $element->distanceDeLaSource <= $dist && $element->visited == false) {
            $dist = $element->distanceDeLaSource;
            $res = $element;
        }
    }

    return $res;
}

/**
 * @param string $id
 * @param Graph[] $listeNoeuds
 * @return null|Graph
 */
function getNoeud($id, $listeNoeuds){
    $res = null;
    foreach ( $listeNoeuds as $element ) {
        if ( $element->id == $id) {
            return $element;
        }
    }
    return $res;
}


/**
 * @param Graph[] $listofnodes
 */
function initialiseGraph(&$listofnodes) {
    foreach ($listofnodes as $node){
        $node->visited = false;
        $node->distanceDeLaSource = INF;
        $node->meilleurParentDepuisSource = null;
    }
}

/**
 * @param Graph $noeudSource
 * @param Graph[] $listeNoeuds
 */
function dijkstra(&$noeudSource,&$listeNoeuds){
    $noeudSource->distanceDeLaSource = 0;
    $noeudActuel = $noeudSource;
    while ($noeudActuel != null){
        $noeudActuel->visited = true;
        foreach ($noeudActuel->branches as $branche){
            if($branche->destination->distanceDeLaSource > $noeudActuel->distanceDeLaSource + $branche->poids){
                $branche->destination->distanceDeLaSource = $noeudActuel->distanceDeLaSource + $branche->poids;
                $branche->destination->meilleurParentDepuisSource = $noeudActuel;
            }
        }
        $noeudActuel = rechercheNoeudPlusPetiteDistanceNonVisite($listeNoeuds);
    }
}

/**
 * @param Graph $noeudSource
 * @param Graph[] $listeNoeuds
 */
function bellmanFord(&$noeudSource,&$listeNoeuds){
    $noeudSource->distanceDeLaSource = 0;

    foreach ($listeNoeuds as $noeud){
        foreach ($noeud->branches as $branche){
            if($noeud->distanceDeLaSource != INF && $noeud->distanceDeLaSource + $branche->poids < $branche->destination->distanceDeLaSource){
                $branche->destination->distanceDeLaSource = $noeud->distanceDeLaSource + $branche->poids;
                $branche->destination->meilleurParentDepuisSource = $noeud;
            }
        }
    }
}

/**
 * @param Graph $noeudDepart
 * @param Graph $noeudArrivee
 * @param Graph [] $listeNoeuds
 * @return null|array
 */
function plusCourtCheminDijkstra(&$noeudDepart,&$noeudArrivee,&$listeNoeuds){

    initialiseGraph($listeNoeuds);
    dijkstra($noeudArrivee,$listeNoeuds);

    $res = array(
        'chemin ' => array(),
        'distance' => $noeudDepart->distanceDeLaSource,
    );

    $noeudActuel = $noeudDepart;

    while ($noeudActuel != null){
        $res['chemin'][] = $noeudActuel->id;
        $noeudActuel = $noeudActuel->meilleurParentDepuisSource;
    }

    return $res;
}




/**
 * @param Graph $noeudDepart
 * @param Graph $noeudArrivee
 * @param Graph [] $listeNoeuds
 * @return null|array
 */
function plusCourtCheminBellmanFord(&$noeudDepart,&$noeudArrivee,&$listeNoeuds){

    initialiseGraph($listeNoeuds);
    bellmanFord($noeudArrivee,$listeNoeuds);

    $res = array(
        'chemin ' => array(),
        'distance' => $noeudDepart->distanceDeLaSource,
    );

    $noeudActuel = $noeudDepart;

    while ($noeudActuel != null){
        $res['chemin'][] = $noeudActuel->id;
        $noeudActuel = $noeudActuel->meilleurParentDepuisSource;
    }

    return $res;
}